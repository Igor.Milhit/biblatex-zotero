---
title: Citation et bibliographie avec LaTeX
date: 2023-03-21T10:22:47+01:00
id: 20230321102247
tags: [README, documentation, latex, rdv-info]
---

## [Citation et bibliographie avec LaTeX][1]

Ce répertoire contient les sources pour le *Rendez-vous de l'info* sur les
citations et les références bibliographiques avec LaTeX. Il contient :

- La source en markdown pour le support de cours (`sources/polycopié/`).
- Des exemples de fichiers `.tex` (`exercice/exercice.tex`).
- Un export de collection Zotero au format biblatex
  (`exercice/references-exercices.bib`).

Le support de cours au format PDF ainsi que les fichiers d'exercice peuvent
être téléchargés dans [les publications de version][2].

## Prérequis

Pour pouvoir utiliser ce projet, il est nécessaire :

1. De cloner le projet : \
   `git clone https://git.milhit.ch/igor/biblatex-zotero.git`
1. Pour générer les différents fichiers, les outils suivants sont nécessaires :
   1. Soit `pagedjs`. Pour installer `pagedjs` : \ 
      `npm install -g puppeteer pagedjs pagedjs-cli`.
   1. Soit `pandoc` et LaTex. Pour installer `pandoc`, voir [la
      documentation][3]. Pour la distribution LaTeX, une possibilité est de
      consulter le site web [*The LaTeX project*][4] qui décline les méthodes
      les plus habituelles pour les principaux systèmes d'exploitation. **Pour
      ma part, je recommande l'usage de `tectonic` qui est un binaire à
      installer** : voir comment [installer tectonic][5].

## Convertir le support de cours en PDF

### Avec `paged.js`

Avec cet outil, il est possible de définir les styles des éléments dans le PDF
au moyen de règles CSS (voir [paged.js][6]). Dans le dossier
`sources/polycopié/` se trouvent un `template.html` utilisé par `pandoc` pour
produire un fichier HTML avec les classes et les identifiants désirés. Le
fichier `style.css` contient pour sa part la mise en forme permettant d'obtenir
un fichier suivant en bonne partie la charte de l'UNIGE.

Pour pouvoir utiliser `pagedjs`, il faut avoir installé sur sa machine les
paquets `npm` suivants :

```bash
npm install -g puppeteer pagedjs pagedjs-cli
```

Puis, on peut produire directement un PDF :

```bash
cd sources/polycopié/
pandoc --standalone --embed-resource --citeproc \
       --to=pdf --pdf-engine=pagedjs-cli \
       --toc --toc-depth=2
       --css=style.css \
       --output=../../public/polycopié.pdf polycopié.md
```

Pour obtenir un fichier HTML, qui peut être ensuite imprimé au format PDF :

```bash
cd sources/polycopié/
pandoc --citeproc --to=html \
       --template=template.html --css=style.css \
       --output=polycopié.html polycopié.md
```

Puis, lancer un server web local depuis la racine du projet, par exemple avec
`python -m http.server` et charger le fichier `polycopié.html` dans un
navigateur web, de préférence Chromium (ou Google Chrome), car la gestion des
liens est mieux supportée. Un fois le fichier chargé, il faut utiliser la
fonction d'impression du navigateur, au format PDF.

### Avec LaTeX

L'usage de LaTeX suppose une distribution LaTeX. Le résultat obtenu n'est pas
adapté à la charte de l'UNIGE, et n'est pas même optimal.

Pour convertir le support de cours (`polycopié.md`) au format PDF, il faut
utiliser la commande suivante :

```bash
pandoc --citeproc --to=pdf --pdf-engine=tectonic \
       --output=public/polycopié.pdf \
       source/polycopié/polycopié.md
```

- `--citeproc` traite les citations et génère la bibliographie.
- `--to=pdf` détermine le format de sortie, ici PDF.
- `--pdf-engine` définit le moteur de compilation a utiliser pour produire le
  PDF, ici `tectonic`, mais `xelatex` devrait fonctionner aussi, si il est
  installé.
- `--output=` spécifie le chemin et le nom du fichier généré.

## [Déroulé de la présentation][7]

Le fichier `déroulé.md` décrit le [déroulé de la présentation][7]. Si une
impression est nécessaire, on peut soit utiliser le rendu HTML d'un éditeur,
soit le convertir au format désiré puis l'imprimer.

Pour le HTML :

```bash
pandoc -t html5 -s -o déroulé.html déroulé.md
```

Pour le PDF :

```bash
pandoc -t pdf --pdf-engine=tectonic -o déroulé.pdf déroulé.md
```

<!-- références -->
[1]: ./
[2]: https://git.milhit.ch/igor/biblatex-zotero/releases/latest
[3]: https://pandoc.org/installing.html
[4]: https://www.latex-project.org/get/
[5]: https://tectonic-typesetting.github.io/en-US/install.html
[6]: https://pagedjs.org/
[7]: ./déroulé.md
