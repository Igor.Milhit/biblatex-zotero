---
title: Déroulé de la présentation LaTeX et citation
date: 2023-03-29T15:44:49+02:00
id: 20230329154459
tags: [latex, déroulé, rdv-info]
---

## Vérifier

- [ ] Avoir un dossier de travail avec les fichiers :
  - `exercice.tex`.
  - `references-exercice.bib`
  - `references-exercice.ris`
- [ ] Que la compilation rapide fonctionne soit avec PdfLaTeX ou XeLaTeX.
- [ ] Que BibLaTeX soit sur `bibtex %`.
- [ ] Imprimer le polycopié et la *cheatsheet*. Ou préparer les liens vers les
    documents pour une session en visio.

## Déroulé

### Introduction

1. Questions aux personnes :
    1. Contexte académique.
    1. Usage de LaTeX.
    1. Quel environnement LaTeX.
    1. Usage d'un gestionnaire de référence.
1. Plan.
1. Objectifs.
1. Principes de base.
    1. Langage de balisage dans un fichier `.tex`.
    1. Système de composition.
    1. Éditeur de texte ou éditeur dédié.
    1. Base de donnée des références.
    1. BibTeX VS BibLaTeX.
1. Structure d'une référence

### Exercice

1. Proposer d'importer `references-exercice.ris`.
1. Export Zotero.
1. Better BibTeX. Exporter et montrer la liste des exportations.
1. Ouvrir le fichier `exercice.tex` avec Texmaker.
1. Montrer :
    1. Commentaires, dont l'insertion de la bibliographie.
    1. Préambule avec les paquets et les options utilisés.
        1. Dont biblatex, ses options et la base de donnée de références.
    1. Structure du document.
    1. Commandes.
1. Montrer comment configurer la compilation dans Texmaker.
    1. `biber %` pour BibLaTeX.
    1. Compilation rapide avec :
        1. Soit préconfiguration PdfLaTeX, Biber, PdfLaTeX 2x et affichage du
           PDF.
        1. Soit, avec l'assistant XeLaTeX, Biber, XeLaTeX 2x et affichage du
           PDF.
1. Compiler le fichier tel quel.
1. **Laisser faire.**
1. Montrer les fichiers auxiliaires.
1. Montrer qu'on peut les supprimer avec *Outils / nettoyer* dans Texmaker.
1. Ajouter un appel à citation simple (`\cite`), décommenter l'insertion de la
   bibliographie.
1. Compiler et **Laisser faire.**
1. Changer le `\cite` en `\autocite`.
1. Essayer d'autres commandes de citation via la cheatsheet.
    1. Ajouter des options à `\autocite` et souligner que les deux options
       doivent être affichées :
       `\autocite[voir][]{bohemierBibTeXNatbibBiblatex}`.
    1. Insérer un `\textcite{bosmaCodeswitchingAsymmetryBilingual2019}`.
    1. Insérer un `\parencite{caiWhatMakesGood2021}`.
1. Compiler et **Laisser faire.**
1. Insérer une référence multiple :
   `\textcites{bosmaCodeswitchingAsymmetryBilingual2019}{sauvayreMethodesEntretienSciences2013}` 
1. Compiler et **Laisser faire.**
1. Passer à un style `numeric`, `apa`, `vancouver`. Signaler qu'il faudra
   peut-être installer un style. Mentionner le topic biblatex.
1. Compiler et **Laisser faire.**
