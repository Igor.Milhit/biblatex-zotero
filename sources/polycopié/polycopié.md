---
title: "Citer ses références et créer une bibliographie avec LaTeX et Zotero"
date: Automne 2024
creation_date: 2023-03-24T08:25:47+0100
id: 20230324082557
logo:
  - ../medias/latex-logo.svg
  - ../medias/zotero-logo.png
lang: fr
bibliography: references.json
csl: heg-iso-690.csl
nocite: '@*'
link-citations: true
---

## Objectifs

Au terme de cet atelier, vous serez en mesure de :

- Comprendre ce qu'est un fichier `.bib`.
- Connaître la structure d'une référence.
- Créer ou de générer un fichier `.bib`.
- Charger les paquets nécessaires dans le préambule pour configurer BibLaTeX.
- Utiliser les commandes BibLaTeX pour :
  - Définir le style de citation et de bibliographie.
  - Insérer une citation et un appel de citation.
  - Insérer une bibliographie.
- Compiler le document final.

:::{#licence}
![Logo de la licence CC BY-SA 4.0][1] \
[CC BY-SA 4.0][2] --- [Sources][3]
:::

<!-- références -->

[1]: ./static/by-sa.svg
[2]: https://creativecommons.org/licenses/by-sa/4.0/deed.fr "Texte de la licence en français"
[3]: https://git.milhit.ch/igor/biblatex-zotero "Projet git du document"

## Principes généraux {#first-section}

> « Dans LaTeX il faut distinguer le langage de balisage, le système de
> composition et la *distribution*. » [@fauchieFabriquesPublicationLaTeX2020]

Pour générer des documents avec LaTeX, il faut :

- Rédiger un document dans un fichier `.tex` à l'aide du *langage de balisage*,
  afin de le structurer.
- Compiler ce fichier avec un *système (ou moteur) de composition*, par exemple
  LaTeX, PdfLaTeX ou XeLaTeX.

Pour disposer d'un tel système de composition, le plus souvent on installe un
*environnement ou une distribution* LaTeX, par exemple TeX Live pour Linux,
MacTex pour Mac OS ou MiKTeX pour Windows[^0]. Il est aussi possible
d'installer des distributions plus minimales, comme `tectonic` ou TinyTeX[^6].

Afin de faciliter la collaboration ou pour s'éviter une installation parfois
laborieuse, des services en ligne sont préférés, comme
<https://www.overleaf.com/>.

Enfin, un éditeur est nécessaire pour travailler sur les sources (le ou les
fichiers `.tex`). N'importe quel éditeur de texte suffit, mais des aides comme
la coloration syntaxique, la complétion des commandes ou la gestion de la
compilation autrement que dans une console sont très utiles. Parmi une liste
plutôt riche, les éditeurs suivants peuvent être mentionnés :

- Texmaker, libre, multiplatforme, <https://www.xm1math.net/texmaker/>.
- TeXworks, libre, multiplatforme, <https://www.tug.org/texworks/>.
- Visual Studio Code, gratuit, multiplatforme,
  <https://code.visualstudio.com/>, avec les extensions nécessaires.

Afin de pouvoir insérer des citations et créer des bibliographies avec LaTeX,
un fichier contenant les références bibliographiques est nécessaire. Il s'agit
d'un fichier dont l'extension est `.bib`, ce qui correspond :

- soit à la structure BibTeX,
- soit à la structure Biber pour BibLaTeX.

Ce fichier `.bib` se place le plus souvent dans le même dossier que le document
`.tex` de travail, mais il est possible de l'enregistrer n'importe où et de
préciser son chemin dans le fichier `tex`.

<!-- références -->

[^0]: Voir le site <https://www.latex-project.org/get/>.
[^6]: Voir le site <https://tectonic-typesetting.github.io> ou
    le site <https://yihui.org/tinytex/>.

### BibTeX VS BibLaTeX {#bibtex-vs-biblatex .newpage}

BibTex est le logiciel historique pour la gestion des références et des
bibliographies dans un document `.tex`. Il est aussi le plus souvent mentionné
dans les exigences des éditeurs scientifiques. Pourtant, ce logiciel n'est plus
développé depuis de plusieurs décennies et n'offre pas facilement les
fonctionnalités auxquelles nous pouvons nous attendre actuellement.

Aussi Biber (et BibLaTeX) est fortement conseillé et c'est pourquoi il n'est
question que de cette option dans ce document. Il est plus récent et évolue
encore constamment. En ne chargeant que le paquet `biblatex`, il est possible
de définir le type d'appel de citation (notes en bas de page, numéro,
auteur-date, etc.) et différents style de bibliographies.

Débuter avec BibLaTeX n'empêche pas de passer à BibTeX ensuite.

## Structure d'une référence dans un fichier `.bib`

Une référence dans un fichier `.bib` au format BibLaTeX ressemble à ce qui
suit :

```bib
@book{sauvayreMethodesEntretienSciences2013,
  location = {Paris},
  title = {Les méthodes de l'entretien en sciences sociales},
  isbn = {978-2-10-057970-9},
  series = {Psycho sup. Psychologie sociale},
  pagetotal = {138},
  publisher = {Dunod},
  author = {Sauvayre, Romy},
  date = {2013},
  keywords = {Enquêtes sociologiques, Entretiens, Sociologie},
}
```

La première ligne définit le type de document, sous la forme `@type` (`@book`,
`@article`, `@phdthesis`, etc.). Puis, après l'ouverture d'accolade, suit la
clé de citation (*Citekey*), c'est-à-dire l'identifiant unique de la référence
qui sera indiquée dans le document `.tex`, afin d'insérer une référence. \
Les lignes suivantes correspondent aux champs de la référence, sous la forme
`champ = {valeur}`, séparées par une virgule.

La documentation BibLaTeX détaille l'ensemble des types et des champs
[@lehmanBiblatexPackageProgrammable2023, p. 7]. Une *cheatsheet* les liste
également [voir @reesBibLaTeXBiberCheat2017].

Il est possible de créer et de mettre à jour un fichier `.bib` à la main, à
l'aide d'un éditeur de texte. Un grand nombre de bases de données, de sites de
revues ou de catalogues de bibliothèques permettent de télécharger une ou
plusieurs références dans un fichier `.bib`, mais le plus souvent dans la
structure BibTeX et non pas BibLaTeX.

Mais, il est recommandé de déléguer la gestion du fichier `.bib` à un logiciel
de gestion de références bibliographiques, comme JabRef[^1] ou Zotero[^2].
L'intérêt de ces logiciels est d'éviter les erreurs et d'automatiser en partie
la collecte de références.

<!-- références -->

[^1]: Logiciel multiplatforme et libre, supporte BibTeX et Biber nativement. <https://www.jabref.org/> 
[^2]: Logiciel multiplatforme et libre. <https://www.zotero.org/>

## Créer un fichier `.bib` via Zotero

Avec Zotero, il est possible de générer un fichier `.bib` :

- À partir d'une sélection de références.
- En sélectionnant une collection, une sous-collection ou la bibliothèque
  entière.

Il suffit de :

- Sélectionner l'ensemble désiré.
- Faire un clic-droit et sélectionner *Exporter la collection / les documents*.
- Choisir le format BibTeX ou BibLaTeX.[^3]

Ce procédé très simple a néanmoins un inconvénient : le fichier créé est
statique. Il ne sera pas mis à jour automatiquement si la collection est
complétée, ou si les références sont corrigées dans Zotero. Il faudra générer à
nouveau et écraser le fichier `.bib` pour qu'il soit à jour.

[^3]: Il est également possible d'exporter une bibliothèque au format
    BibTeX depuis d'autres logiciels bibliographiques, notamment
    EndNote. Pour cela, dans ce dernier, il faut aller dans les options
    de styles *Tools / Output Styles / Open Style Manager* et cocher le
    style *BibTeX Export*. Une fois cela fait, il faut cliquer sur *File
    Export*, sélectionner le style *BibTeX Export* et sauvegarder le
    fichier au format `.bib`.

### L'extension *Better BibTeX for Zotero*

L'extension *Better BibTeX for Zotero* résout deux problèmes des fichiers
`.bib` générés par Zotero :

1. Elle permet d'exporter **un fichier `.bib` dynamique, c'est-à-dire que le
   contenu de celui-ci sera mis à jour automatiquement lorsque la collection
   correspondante dans Zotero sera modifiée**. Pour cela, il faut choisir le
   format d'exportation *Better BibTeX* ou *Better BibLaTeX* et sélectionner
   l'option *Garder à jour*. (Figure 1) \
1. Elle permet de **modifier une clé de citation (citekey) manuellement ou de
   créer un format de citekey par défaut.**

![Fenêtre de dialogue de Better BibTex](../medias/better-bibtex-a-jour.png)

Pour installer l'extension (au format `.xpi`) *Better BibTeX* :

- Se rendre sur <https://github.com/retorquere/zotero-better-bibtex/releases/latest>.
- Télécharger le fichier `Zotero-better-bibtex-X.X.X.xpi`.
- Dans Zotero, choisir le menu *Outils / Extensions* (Figure 2).
- Cliquer sur la roue crantée en haut à droite et sélectionner *Install Add-on
  from file* (Figure 3).
- Sélectionner le fichier téléchargé.

![Menu « outils » de Zotero](../medias/menu-outils.png)

![Installer un fichier `.xpi`](../medias/install-add-on.png)

### Autres fonctionnalités de Better BibTeX

*Better BibTeX* propose des fonctionnalités plus avancées :

- Création et gestion de clés uniques.
- Conversion des caractères UTF-8 et des éventuels balisages HTML
  présents dans les références.
- Génération de champs supplémentaires absents de Zotero.
- Création de types de documents n'existant pas dans Zotero.

Ces fonctionnalités ne seront pas décrites en détail dans ce document.
Les informations supplémentaires à ce sujet sont disponibles sur le site
<https://retorque.re/zotero-better-BibTeX/>.

## Configurer la compilation

Pour générer le fichier PDF à partir des sources `.tex` et `.bib`, il est
nécessaire de configurer le système de composition que l'on va utiliser. Il
peut être appelé en ligne de commande dans une console ou, à l'inverse, pris en
charge presque entièrement par une plateforme comme Overleaf. Et entre les
deux, des éditeurs de texte dédiés à LaTeX offrent un certain nombre d'options
de compilation ou de composition.

L'exemple donné ci-dessous est adapté à l'exercice et à l'utilisation de
l'éditeur Texmaker, mais le principe est similaire dans les différents
environnement.

Dans Texmaker, la fonction pour lancer BibTeX ou Biber (BibLaTeX) est la même.
Il faut donc lui préciser lequel on utilise :

1. Menu Options.
1. Configurer Texmaker.
1. Dans l'onglet *Commandes*, chercher l'entrée *Bib(la)tex* et entrer
   `biber %` (Figure 4).

![Configuer Biber pour BibLaTeX dans Texmaker](../medias/texmaker-biber.png)

Lorsque l'on compile avec des références bibliographiques, il est nécessaire de
le faire quatre fois :

1. XeLaTeX.
1. Biber.
1. XeLaTeX.
1. XeLaTeX.
1. Afficher le PDF.

Heureusement Texmaker permet de configurer une *Compilation rapide* qui lance
automatiquement ces quatre itérations :

1. Menu Options.
1. Configurer Texmaker.
1. Onglet *Compil rapide*.
1. Choisir la suite de commande à lancer.

Dans le cas de l'exercice présenté, une bonne solution est d'utiliser XeLaTeX.
Pour cela, il faut choisir l'option *Utilisateur* et utiliser l'assistant
(figure 5).

![Assistant de configuration de compil rapide](../medias/compil-rapide-assistant.png)

Puis, dans l'assistant, sélectionner les commandes à ajouter, une à une, dans
l'ordre (figure 6).

![Ajout des commandes dans l'assistant](../medias/compil-rapide-liste.png)

Les services web comme Overleaf ou les outils comme `tectonic`[^5] prennent
eux-mêmes en charge ce genre de complexité.

[^5]: Voir <https://tectonic-typesetting.github.io>. L'intérêt de `tectonic`
est qu'il s'occupe de télécharger à la volée des logiciels nécessaires en
fonction de ce qui est indiqué dans le préambule, ce qui évite d'installer une
distribution de LaTeX volumineuse. À l'exception de Biber qui doit être
installé sur le système.

## Utilisation de BibLaTeX

### Préambule : options de style et lien avec les références {#preambule} 

Voici un exemple de préambule avec le paquet `biblatex` et quelques options :

```latex
\documentclass[11pt,a4paper,french]{article}
\usepackage{lmodern}
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{hyperref}
\usepackage[style=vancouver,
            backend=biber]{biblatex}
\addbibresource{references-exercice.bib}

\author{Dimitri Donzé, Vincent Hubert, Igor Milhit}
\title{Notre pseudo-article}
```

Le paquet est chargé sous la forme `\usepackage[options]{biblatex}`. Pour les
options, il faut se référer à la documentation ou à la *cheatsheet* citées plus
haut. L'exemple montre les options que vous allez le plus souvent utiliser :

- `backend=biber` détermine l'outil de composition, le plus souvent Biber pour
  BibLaTeX. Cela suppose que Biber soit installé sur votre machine, par exemple
  dans la distribution LaTeX que vous avez choisie.
- `style` détermine le style dans lequel les références seront rédigées.

Pour plus de finesse, existent les options `citestyle` pour définit le style de
citation et `bibstyle` pour le style bibliographique. Le plus souvent l'option
`style` suffit.

La commande `\addbibresource{<nom-du-fichier>.bib}` permet de définir le chemin
et le fichier contenant les références bibliographiques. Le plus simple, est de
le placer dans le même répertoire que le fichier `.tex`.

### Insérer des citations

Pour insérer des citations, il existe un ensemble de commandes possibles (voir
la *cheatsheet*), pour les différents cas. La syntaxe est la suivante :

```latex
\cite[<pre>][<post>]{<key>}
```

- `\cite` est la commande proprement dite, qui détermine le type de citation
  utilisée (`\cite`, `\autocite`, etc.).
- `[<pre>]`, optionnel, permet d'ajouter du texte avant l'appel à citation. Par
  exemple `[voir]` permet d'obtenir `(voir Auteur, date)`.
- `[<post>]`, optionnel, ajoute du texte après l'appel à citation, comme un
  numérotation de pages.
- `{<key>}`, obligatoire, est la clé de citation indiquant de quelle référence
  il s'agit. Par exemple `{slaweckiParadigmsQualitativeResearch2018}`.

Voici les appels de citation les plus utiles :

- `\autocite` est dépendante du style utilisé, aussi elle s'adapte
  automatiquement au style de citation.
- `\textcite` insère l'appel à citation dans le texte.
- `\parencite` insère l'appel à citation dans des parenthèses.

Ces commandes possèdent des versions pour appeler plusieurs références
simultanément : `\autocites{<key1>}{<key2>}`.

### Créer une bibliographie

La commande `\printbibliography` insère la bibliographie. Elle se place
généralement à la fin du document, mais ce n'est pas obligatoire.

Elle peut prendre un certain nombre d'options, par exemple :

```latex
\printbibliography[title=Bibliographie]
```

Cet exemple définit le titre de la bibliographie. Par défaut, le titre est
*Références* en français.

À l'aide de la *cheatsheet* ou de la documentation il est possible de
comprendre comment réaliser une bibliographie pour une section du document, ou
une bibliographie ne contenant que les références liées à un mot clé
particulier.

### Enrichir BibLaTeX

Dans la bibliographie est indiqué une ressource en ligne qui liste des paquets
pouvant être installés, afin d'enrichir BibLaTeX [@CTANTopicBibLaTeX]. Dans ces
paquets, on trouve par exemple des styles bibliographiques. Or, pour les
installer, il faut savoir dans quel répertoire il faut ajouter les fichiers, si
nécessaire. Pour cela, une commande peut être tapée dans une console (par
exemple Powershell pour Windows, iterm pour Mac OS ou votre terminal Linux) :

```bash
> kpsewhich --var-value TEXMFLOCAL
/usr/local/share/texmf:/usr/share/texmf
```

La deuxième ligne est, la sortie de la commande, à savoir le chemin
correspondant à l'installation de LaTeX où les répertoires et fichiers peuvent
être ajoutés.

Sous Windows la commande ne prend qu'un tiret pour le paramètre :

```bash
kpsewhich -var-value TEXMFLOCAL
```

## Bibliographie {.newpage}
