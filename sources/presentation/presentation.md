---
title: Citer ses références et créer une bibliographie avec LaTeX
author: iGor milhit
date: 2023-03-30
creation_date: 2023-03-21T09:07:01+01:00
id: 20230321090701
tags: [rdv-info, latex, biblatex, zotero, bibliographie]
---

## Objectifs de la formation

- Exporter ses références de Zotero au format `.bib`.
- Configurer le style de citation et de bibliographie.
- Insérer des citations.
- Créer une bibliographie.

## Exemple de préambule

```latex
\documentclass[11pt,a4paper]{article} 
\usepackage{lmodern}
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{hyperref}
\usepackage[citestyle=numeric,
            style=vancouver,
            backend=biber]{biblatex}
\addbibresource{references.bib}
```

# Examples

## Exemple de bloc

### Titre {.alert}

Texte dans un bloc.
