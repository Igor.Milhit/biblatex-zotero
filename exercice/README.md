---
title: Dossier `exercice`.
date: 2023-03-29T08:45:28+02:00
id: 20230329084537
tags: [latex, exercice]
---

## Exercice

Dans ce dossier se trouvent deux fichiers qui seront utilisés au cours de la
formation :

- `exercice.tex`, un pseudo article minimal, en français, dans lequel il faut
  ajoute des appels à citation et des références bibliographiques.
- `references-exercice.bib`, la base de données des références bibliographiques
  utilisées dans l'article.

Pour réaliser l'exercice, il faut un environnement LaTeX disposant de Biber et
de pdfLaTeX ou XeLaTeX, si l'on veut travailler en local. Une autre solution
est d'utiliser la plateforme <https://www.overleaf.com/>.

